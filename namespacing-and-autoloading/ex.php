<?php
 /**
 * Author: Cory Norris
 * Created At: 14/06/15, 9:59 PM
 */

use Acme\Business;
use Acme\Person;
use Acme\Staff;

$cory = new Person("Cory");

$staff = new Staff([$cory]);

$microsoft = new Business($staff);

$microsoft->hire(new Person('Jane Doe'));

var_dump($microsoft->getStaffMembers());
