<?php

class Person
{

    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}

// Depends on Staff
class Business
{

    protected $staff;


    public function __construct($staff)
    {
        $this->staff = $staff;
    }

    public function hire(Person $person)
    {
        $this->staff->add($person);
    }

    public function getStaffMembers()
    {
        return $this->staff->members();
    }

}


class Staff
{

    protected $members;

    function __construct($members = [])
    {
        $this->members = $members;
    }


    public function add(Person $person)
    {
        $this->members[] = $person;
    }

    public function members()
    {
        return $this->members;
    }

}


$cory = new Person("Cory");

$staff = new Staff([$cory]);

$microsoft = new Business($staff);

$microsoft->hire(new Person('Jane Doe'));

var_dump($microsoft->getStaffMembers());
