<?php

class Math {

    public $normal = 1;
    public static $static_value = 1;

    public function add(...$nums)
    {
        return __FUNCTION__.':'.array_sum($nums);
    }


    public static function static_add(...$nums)
    {
        return __FUNCTION__.':'.array_sum($nums);
    }


}

$math = new Math;

$math2 = new Math;
echo $math->add(1,2,3,4);
echo '<br/>';
echo Math::static_add(1,2,3,4);
echo '<br/>';
echo $math->normal;
echo '<br/>';
echo Math::$static_value;
Math::$static_value += 2;
echo '<br/>';
echo Math::$static_value;
